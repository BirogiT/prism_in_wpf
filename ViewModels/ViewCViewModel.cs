﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using PSamples.Service;
using PSamples.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace PSamples.ViewModels
{
  public class ViewCViewModel : BindableBase, IDialogAware
  {
    IDialogService _dialogService;
    IMessageService _messageService; //①

    // ④
    public ViewCViewModel(IDialogService dialogService)
      : this(dialogService, new MessageService())
    {
    }

    public ViewCViewModel(
      IDialogService dialogService,
      IMessageService messageService) //②
    {
      _dialogService = dialogService;
      _messageService = messageService; //③
      OKButton = new DelegateCommand(OKButtonExecute);
    }

    private string _viewCTextBox = "XXX";

    public event Action<IDialogResult> RequestClose;

    public string ViewCTextBox
    {
      get { return _viewCTextBox; }
      set { SetProperty(ref _viewCTextBox, value); }
    }

    public string Title => "ViewCのタイトル";

    public DelegateCommand OKButton { get; }

    public bool CanCloseDialog()
    {
      return true;
    }

    public void OnDialogClosed()
    {
    }

    public void OnDialogOpened(IDialogParameters parameters)
    {
      ViewCTextBox = parameters.GetValue<string>(nameof(ViewCTextBox));
    }

    private void OKButtonExecute()
    {
      //MessageBox.Show("Saveします");

      //var message = new DialogParameters();
      //message.Add(nameof(MessageBoxViewModel.Message), "Saveします");
      //_dialogService.ShowDialog(nameof(MessageBoxView), message, null);

      // ⑤
      if (_messageService.Question("保存しますか?") == MessageBoxResult.OK)
      {
        _messageService.ShowDialog("Saveしました。");
      }

      var p = new DialogParameters();
      p.Add(nameof(ViewCTextBox), ViewCTextBox);
      RequestClose?.Invoke(new DialogResult(ButtonResult.OK, p));
    }
  }
}
