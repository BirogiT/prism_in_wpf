﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using PSamples.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PSamples.ViewModels
{
  public class ViewBViewModel : BindableBase, INavigationAware, IConfirmNavigationRequest //①
  {
    IMessageService _messageService; //③

    public ViewBViewModel() : this(new MessageService()) //⑤
    {
    }

    public ViewBViewModel(IMessageService messageService) //④
    {
      _messageService = messageService; //④
    }

    private string _myLabel = string.Empty;
    public string MyLabel
    {
      get { return _myLabel; }
      set { SetProperty(ref _myLabel, value); }
    }

    public void OnNavigatedTo(NavigationContext navigationContext)
    {
      MyLabel = navigationContext.Parameters.GetValue<string>(nameof(MyLabel));
    }

    public bool IsNavigationTarget(NavigationContext navigationContext)
    {
      return true;
    }

    public void OnNavigatedFrom(NavigationContext navigationContext)
    {
    }

    //②自動生成で作成、NotImplementedExceptionは削除する。
    public void ConfirmNavigationRequest(NavigationContext navigationContext, Action<bool> continuationCallback)
    {
      //⑥
      if(_messageService.Question("保存せずとじますか？") == System.Windows.MessageBoxResult.OK)
      {
        continuationCallback(true);
      }
    }
  }
}
