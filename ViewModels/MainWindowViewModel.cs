﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using PSamples.Views;
using System;

namespace PSamples.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IRegionManager _regionManager;
　　    private readonly IDialogService _dialogService;

        //①
        private bool _showAEnabled = false;
        public bool ShowAEnabled
        {
          get { return _showAEnabled; }
          set { SetProperty(ref _showAEnabled, value); }
        }

        private string _title = "PSamples";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public MainWindowViewModel(
          IRegionManager regionManager,
          IDialogService dialogService)
        {
          _regionManager = regionManager;
　　      _dialogService = dialogService;
          SystemDateUpdateButton = new DelegateCommand(SystemDateUpdateButtonExecute);
          ShowViewAButton = new
            DelegateCommand(ShowViewAButtonExecute).ObservesCanExecute(() => ShowAEnabled); //②
          ShowViewBButton = new DelegateCommand(ShowViewBButtonExecute);
      　　ShowViewCButton = new DelegateCommand(ShowViewCButtonExecute);
          ShowViewDButton = new DelegateCommand(ShowViewDButtonExecute);
        }

        private string _systemDateLabel = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        public string SystemDateLabel
        {
          get { return _systemDateLabel; }
          set { SetProperty(ref _systemDateLabel, value); }
        }

        public DelegateCommand SystemDateUpdateButton { get; }

        public DelegateCommand ShowViewAButton { get; }
        public DelegateCommand ShowViewBButton { get; }
        public DelegateCommand ShowViewCButton { get; }
        public DelegateCommand ShowViewDButton { get; }

        private void SystemDateUpdateButtonExecute()
        {
          ShowAEnabled = true; //③
          SystemDateLabel = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        }

        private void ShowViewAButtonExecute()
        {
          _regionManager.RequestNavigate("ContentRegion", nameof(ViewA));
        }
        private void ShowViewBButtonExecute()
        {
          var p = new NavigationParameters();
          p.Add(nameof(ViewBViewModel.MyLabel), SystemDateLabel);
          _regionManager.RequestNavigate("ContentRegion", nameof(ViewB), p);
        }

        private void ShowViewCButtonExecute()
        {
          var p = new DialogParameters();
          p.Add(nameof(ViewCViewModel.ViewCTextBox), SystemDateLabel);
          _dialogService.ShowDialog(nameof(ViewC), p, ViewCClose);
        }

        private void ViewCClose(IDialogResult dialogResult)
        {
          if (dialogResult.Result == ButtonResult.OK)
          {
            SystemDateLabel =
              dialogResult.Parameters.GetValue<string>(nameof(ViewCViewModel.ViewCTextBox));
          }
        }

        private void ShowViewDButtonExecute()
        {
          _regionManager.RequestNavigate("ContentRegion", nameof(ViewD));
        }
    }
}
